import { BrowserModule }     from '@angular/platform-browser';
import { NgModule }          from '@angular/core';
import { HttpModule }        from '@angular/http'

import { AppComponent }      from './app.component';

import { SocialComponent } from './components/footer/social/social.component';
import { LinksComponent } from './components/footer/links/links.component';
import { AddressComponent } from './components/footer/address/address.component';
import { ChefsComponent } from './components/main/chefs/chefs.component';
import { MenuItemsComponent } from './components/main/menu-items/menu-items.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RoutingModule } from './routing.module';
import { MainComponent } from './components/main/main.component';
import { MenuItemsService } from './components/main/menu-items/menu-items.service';
import { PromoDishComponent } from './components/main/promo-dish/promo-dish.component';
import { MonthlyPromosComponent } from './components/main/monthly-promos/monthly-promos.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ChefsComponent,
    AddressComponent,
    PromoDishComponent,
    MonthlyPromosComponent,
    LinksComponent,
    SocialComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    MenuItemsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RoutingModule
  ],
  providers: [MenuItemsService],
  bootstrap: [AppComponent]
})

export class AppModule { }
