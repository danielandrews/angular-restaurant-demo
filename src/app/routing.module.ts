import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { MenuItemsComponent } from './components/main/menu-items/menu-items.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: '/main', pathMatch: 'full' },
      { path: 'about', component: MainComponent },
    	{ path: 'main', component: MainComponent },
    	{ path: 'menu/:type', component: MenuItemsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class RoutingModule { }
