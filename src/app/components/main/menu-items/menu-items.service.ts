import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs';
import                         'rxjs/add/operator/map';

@Injectable()
export class MenuItemsService {

  startersUrl : string = '/mock-api/menu-items-starters.json';
  mainCoursesUrl: string = '/mock-api/menu-items-main-courses.json';

  constructor(private http: Http) { }

  getStarters(): Observable<any[]> {
    return this.http.get(this.startersUrl).map((res:Response) => res.json());
  }

  getMainCourses(): Observable<any[]> {
    return this.http.get(this.mainCoursesUrl).map((res:Response) => res.json());
  }

}
