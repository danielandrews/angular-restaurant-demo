import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MenuItemsService } from './menu-items.service';

@Component({
  selector: 'restaurant-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.scss']
})

export class MenuItemsComponent implements OnInit {

  type: string;
  starters: Observable<any[]>;
  mainCourses: Observable<any[]>;

  constructor(private menuService: MenuItemsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.starters = this.menuService.getStarters();
    this.mainCourses = this.menuService.getMainCourses();

    this.route.params.subscribe(params => {
      this.type = params['type'];
    });

  }
}

