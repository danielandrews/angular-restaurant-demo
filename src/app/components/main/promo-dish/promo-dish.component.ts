import { Component } from '@angular/core';

@Component({
  selector: 'restaurant-promo-dish',
  templateUrl: './promo-dish.component.html',
  styleUrls: ['./promo-dish.component.scss']
})

export class PromoDishComponent {}
